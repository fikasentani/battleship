let view = {
    displayMessage: function(msg) {
        let messageArea = document.getElementById('messageArea');
        messageArea.textContent = msg;
    },

    displayHit: function(location) {
        let cell = document.getElementById(location);
        cell.setAttribute("class", "hit");
    },

    displayMiss: function(location) {
       let cell = document.getElementById(location);
       cell.setAttribute("class", "miss");
    }
}

view.displayMessage('Mub');
view.displayHit('00');
view.displayHit('32');
view.displayMiss('11');
view.displayMiss('66');

let model = {
    boardSize: 7,
    numShips: 3,
    ships: [
        { locations: ["31", "41", "51"], hits: ["", "", "hit"] },
        { locations: ["14", "24", "34"], hits: ["", "hit", ""] },
        { locations: ["00", "01", "02"], hits: ["hit", "", ""] }
    ],

    // { locations: ["31", "41", "51"], hits: ["", "", ""] },
    //     { locations: ["15", "25", "35"], hits: ["", "", ""] },
    //     { locations: ["00", "01", "02"], hits: ["", "", ""] },

    //     { locations: ["06", "16", "26"], hits: ["", "", ""] },
    //     { locations: ["24", "34", "44"], hits: ["", "", ""] },
    //     { locations: ["10", "11", "12"], hits: ["", "", ""] }

    //Methods

    isSunk: function(ship) {
        for (var i = 0; i < this.shipLength; i++) { //the ship parameter is passed in as an array argument
            if( ship.hits[i] !== "hit") {
                return false;
            }
        }
        return true;
    },

    fire: function(guess) {
        for (var i = 0; i < this.numShips; i++) {
            var ship = this.ships[i];
            console.log('ship ',  this.ships[i] , );
            var locations =  ship.locations;
            console.log('located-->', locations);
            
            var index = locations.indexOf(guess); //returns negative 1 -1 if there is no match
            if (index >= 0) {
                ship.hits[index] = "hit";
                if (this.isSunk(ship)) {
                    this.shipsSunk++;
                }
                return true;
            }
        }
        return false;
    }
}

console.log('the ships are--->', model.ships);

var ship2 = model.ships[1];
var locations2 = ship2.locations;
var hits2 = ship2.hits;
console.log("location is -->", locations2);
console.log("the hits -->", hits2);

var ship3 = model.ships[2];
var hits3 = ship3.hits;
console.log('hits 3 -->', hits3);

if(hits3[0] === "hit") {
    console.log("its a hit");
    
}

var ships1 = model.ships[0];
console.log('ships1 -->', ships1);
var hits1 = ships1.hits;
console.log('hits 1 -->', hits1);
console.log('hits 0 -->', hits1[0]);

hits1[0] = "miss";
console.log('hits 0 -->', hits1[0]);

console.log('first shot -->',model.fire("00"));
console.log('second shot -->',model.fire("02"));




// console.log('first shot -->',model.fire("00"));
// console.log('ships sunk 1 -->',model.shipsSunk);

// console.log('second shot -->',model.fire("02"));
// console.log('ships sunks 2 -->',model.shipsSunk);


